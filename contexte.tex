\chapter{Context}

\begin{spacing}{1.2}
\noindent
In this part, I will quickly define what deep learning is and present the challenge I worked on. 

\section{What is deep learning?}

Deep Learning is a branch of artificial intelligence, which is inspired by the way the brain works. It is a machine learning technique that teaches the computer to filter incoming data by passing it through a neural network to obtain a prediction.

\subsection{Neural Network}

An artificial neural network is composed of different layers of nonlinear processing units for feature extraction and transformation. Each layer takes its inputs from the outputs of the previous one (see Fig. 2.1). At each level, the network learns to transform its input to make the data a little more abstract. The depth of a neural network is defined by the number of layers it contains.

For an image, for example, the input might be a matrix of pixels, then the first layer might encode the edges and compose the pixels, then the next layer might compose an arrangement of edges, then the next layer might encode a nose and eyes, then the next layer might recognize that the image contains a face, and so on. The neural network will define on its own what each layer should extract as information and its degree of abstraction. 

The learning can be supervised or not. Learning is said to be supervised when the network is forced to converge towards a precise final state (and it is this type of learning only that I used during my mission).\\


%inclusion d'une mage dans le document
\begin{figure}[h]
\begin{center}
%taille de l'image en largeur
%remplacer "width" par "height" pour régler la hauteur
\includegraphics[width=0.4\textwidth]{contexte/reseauDeNeurone.png}
\end{center}
%légende de l'image
\caption[]{Simplified vision of an artificial neural network\protect\footnotemark }
\end{figure}

\footnotetext{ Image from \url{https://upload.wikimedia.org/wikipedia/commons/3/3d/Neural_network.svg}}.

\subsection{UNet Model\protect\footnotemark }

\footnotetext{ The UNet model is described in the article \textit{U-Net: Convolutional Networks for Biomedical Image Segmentation} (see ref. \cite{DBLP:journals/corr/RonnebergerFB15})}

There are many different architectures for neural networks. The one I used during my internship is the UNet architecture. This model is used for image segmentation, especially in the medical field. 

The UNet architecture is inspired by convolutional neural networks, used for segmentation and classification. This architecture is represented in figure 2.2. Each block contains a convolution layer (which processes the data of a receiving field), a correction layer, and a pooling layer (which allows compressing of the information). After these different blocks, we obtain a feature map, which is then transformed into a vector by a fully connected layer (and it is this vector that gives a prediction for classification). When we do segmentation, we try to find this vector but also reconstruct the image from this vector. By converting the image into a vector, the network learns the feature map of the image and we will use this map to convert the vector into an image again with the UNet architecture. \\

%inclusion d'une mage dans le document
\begin{figure}[h]
\begin{center}
%taille de l'image en largeur
%remplacer "width" par "height" pour régler la hauteur
\includegraphics[width=0.9\textwidth]{contexte/cnn.jpeg}
\end{center}
%légende de l'image
\caption{Convolutional Neural Network\protect\footnotemark}
\end{figure}

The UNet architecture owes its name to its U shape (see Fig. 2.3). It consists of 3 parts: the contraction, the bottleneck, and the expansion. The contraction part is a traditional convolution network where we alternate convolution, correction, and pooling. The bottleneck is the lowest layer which is the intermediary between the contraction and the expansion. The expansion part is made of several blocks, like the contracting part. To the result, obtained with an up-sampling layer, we add the feature map obtained in the corresponding contraction bloc. That way we can be sure that the attributes learned in the contraction bloc are used to reconstruct the image.


\footnotetext{ Image from \url{https://towardsdatascience.com/a-comprehensive-guide-to-convolutional-neural-networks-the-eli5-way-3bd2b1164a53}}.

\newpage
%inclusion d'une mage dans le document
\begin{figure}[h]
\begin{center}
%taille de l'image en largeur
%remplacer "width" par "height" pour régler la hauteur
\includegraphics[height=10cm]{contexte/u-net-architecture.png}
\end{center}
%légende de l'image
\caption{UNet Architecture\protect\footnotemark}
\end{figure}

\footnotetext{ The image is from the article \textit{U-Net: Convolutional Networks for Biomedical Image Segmentation} (see ref. \cite{DBLP:journals/corr/RonnebergerFB15})}

~\\[0.3cm]
\section{The mission}
\noindent
My mission during those three months was based on the EMIDEC challenge\footnote{ The challenge can be consulted on this website: \url{http://emidec.com/}}.

\subsection{The challenge and its data}
This challenge took place between April and October 2020 but the data used is still available today. Two tasks are proposed: a classification task and a segmentation task. My internship focused on the classification task. 

The patients of the challenge are suspected of having a myocardial infarction. Two-thirds are pathological (they have indeed a myocardial infarction), and the other third is non-pathological. The challenge is to determine whether a patient is pathological or not.

For each patient, we have MRIs of the left ventricle of his heart and annotations that define 5 zones: the background, the myocardium, the cavity, the myocardial infarction, and the no-reflow area (see fig.2.4). The last two labels are present only for pathological patients. 
We also have clinical metadata: the sex of the patient, his age, if he is overweight, his blood pressure, if he is diabetic, his ECG, his troponin level, etc... . We also have access to the diagnosis: we know if the patient is pathological or not. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=0.6\textwidth]{contexte/annotated.png}
\end{center}
\caption{Annotated Imaged Segmented in 5 classes}
\end{figure}

We are working with 100 patients, of which 67 are pathological and 33 are not. 


\subsection{Predicting pathology with metadata alone}
\noindent
We want to know if it is possible to predict whether a patient is pathological or not using only the clinical metadata provided. 

\subsubsection*{Classification using the Troponin level}
~
We have access to the troponin level of each patient. We will therefore choose a threshold above which the patient will be considered as being pathological. 

To choose the optimal threshold value, we use the ROC curve (receiver operating characteristic curve) which displays the true positive rate against the false positive rate. The geometric means are calculated for each threshold: $ gmean = \sqrt{tpr * (1 - fpr)} $ (with \textit{tpr} the true positive rate and \textit{fpr} the false positive rate). The optimal threshold is then the one corresponding to the highest value of the geometric mean. We obtain a threshold of 26.0, for a geometric mean of 0.8841 (see fig. 2.5).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{contexte/troponin.png}
\end{center}
\caption{ROC Curve with the troponin level and the diagnosis}
\end{figure}

Using a threshold of 26.0, we obtained an accuracy of 0.75 on the test dataset. 12 pathological patients and 3 non-pathological patients were correctly diagnosed. This score is slightly higher than the dataset distribution (which is equal to 0.7 since two-thirds of the patients are pathological).

\newpage
\subsubsection*{Results already published}
~
As this challenge was realized in 2020, papers with the results have already been published, both for the segmentation and the classification. 

Among these studies, there is the one conducted by Ana Louren and Eric Kerfoot (see ref. \cite{lourenço2020automatic}). One of the neural networks, named Clinic-NET, takes as input 12 metadata to establish the diagnosis. This network obtains an accuracy of 0.85 (see Fig. 2.6). This score is improved by using MRIs (with the neural network called DOC-NET). 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{contexte/result_challenge.png}
\end{center}
\caption{Results obtained with the metadata}
\end{figure}

\subsection{The mission of the internship}

Following these results, we would like to know if it is possible to diagnose if a patient has a myocardial infarction using only MRIs, without using the clinical metadata. This classification was therefore the core of my mission during these three months of internship. 

\end{spacing}

