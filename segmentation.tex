\chapter{The Segmentation Task}

\begin{spacing}{1.2}

\noindent
The first step for this new approach is to train the network on the segmentation of the DE-MRIs.

\section{The Segmentation on all the labels}
First, I tried to segment all the labels that were available for the dataset: 
\begin{itemize}
    \item the background
    \item the myocardium
    \item the cavity
    \item the myocardial infarction
    \item and the no-reflow area
\end{itemize}

\subsection*{Description of the trainer}
For the segmentation, I used the UNet architecture described in the second chapter. I trained this network for 600 epochs. This training took 5 hours. 

I also used a loss different from the one I was using for the classification task. I decided to use a DiceLoss as it is a common loss for segmentation. 
The DiceLoss is defined as followed (see eq. 4.1), where $TP$ is the number of pixels that were correctly classified as true (True Positives),  $FN$ is the number of pixels that were incorrectly classified as false (False Negatives) and  $FP$ is the number of pixels that were incorrectly classified as true (False Positives). It is a measure of overlap of the predicted mask and the ground truth. 

\begin{equation}
DiceLoss = \frac{2 TP}{2TP + FN + FP}
\end{equation}

\subsection*{Results}
To view the results, I displayed the input, the segmentation map, and the ground truth for each patient. I also computed the dice for each class. 
~\\
The figure 4.1 shows the segmentation map of a pathological patient. We can already see that the first three classes are broadly well segmented, however, the last two classes (myocardial infarction and no-reflow area) have more errors.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{segmentation/all/attention_map.png}
\end{center}
\caption{The Segmentation Map obtained with a segmentation on all labels for a pathological patient}
\end{figure}

\newpage
This result is validated when we look at the dice repartition of all the testing dataset for each class (see fig. 4.2). The score for the background, the myocardium, and the cavity are better than the score for the myocardial infarction and the no-reflow area. Indeed, the dice coefficient outputs a score in the range [0,1] where 1 is a perfect overlap. The closer the dice score is to one, the better the segmentation is.

\begin{figure}[!h]
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
  \hline
  Class & Dice Score & Standard Deviation\\
  \hline
  Background & 0.99 & 0.001\\
  Myocardium & 0.89 & 0.04\\
  Cavity & 0.71 & 0.09\\
  Myocardial Infarction & 0.27 & 0.15\\
  No-reflow area & 0.28 & 0.40\\
  \hline
\end{tabular}
\end{center}
\caption{Chart with the dice score for each class}
\end{figure}

\subsection*{Another strategy for the segmentation}

After the results from this segmentation, we decided to try to segment only the interesting labels. Those labels are the myocardial infarction and the no-reflow area. Indeed there are not empty only for pathological patients and so they might be useful for the classification task. The myocardium and the cavity are now considered as background. The goal of this approach is to decrease the number of annotations and to focus only on what matters. 

\section{The Segmentation only on the interesting labels}

\subsection{The segmentation with a trainer using the DiceLoss}
\noindent
This time, I segmented only on three labels: 
\begin{itemize}
    \item the background (composed of the background, the myocardium, and the cavity)
    \item the myocardial infarction
    \item and the no-reflow area
\end{itemize}

\subsubsection*{Description of the trainer}
I kept the same trainer as before: a UNet model with a Dice for the loss and I still trained it on 600 epochs. The training took 3.9 hours. It took less time than the previous training because there are fewer classes to segment. 

\subsubsection*{The results}
The figure 4.3 shows the segmentation map of both a non-pathological (on the left) and a pathological patient (on the right). This time, the network doesn't manage to segment the myocardial infarction and the no-reflow area. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{segmentation/diceloss/attention_map.png}
\caption{The Segmentation Map obtained using the DiceLoss on the interesting classes for a non pathological patient (left) and a pathological patient (right)}
\end{center}
\end{figure}

Again, the dice confirm these results. The Dice score for the myocardial infarction and the no-reflow area is close to zero which is a really bad result (when we take into account the standard deviation which is relatively high for the no-reflow area). 

\begin{figure}[!h]
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
  \hline
  Class & Dice Score & Standard Deviation\\
  \hline
  Background & 0.99 & 0.00\\
  Myocardial Infarction & 0.19 & 0.40\\
  No-reflow area & 0.50 & 0.50\\
  \hline
\end{tabular}
\end{center}
\caption{Chart with the dice score for each class}
\end{figure}

To improve those results, I tried to change the loss by using the Tversky Loss.

\subsection{The segmentation with a trainer using the TverskyLoss}
\subsubsection*{Description of the TverskyLoss and the trainer}

First, I thought that the issue might come from the imbalanced dataset. The pixels to be segmented represent a very small percentage of the total pixels in the image (or none of the pixels for a non-pathological patient).   

The Tversky Loss\footnotemark ~ is inspired by the Dice Loss.  However, we add weight to the false negatives and the false positives. It enables a finer level of control than the Dice Loss. The Tversky Loss is defined in equation 4.2. The higher the $\alpha$ is, the more we penalize the false negatives. For my application, I wanted to penalize the false positive more, so I decided to work with $\alpha=0.3$. 

\footnotetext{ (see \cite{salehi2017tversky}) }

\begin{equation}
TverskyLoss = \frac{2 TP}{2TP + \alpha FN + (1 - \alpha) FP}
\end{equation}
\\
For this training I still used a UNet model and trained it on 600 epochs. It took X hours. 

\subsubsection*{The results}
However, as it can be seen on the segmentation maps (cf fig. 4.5) and with the dice scores (cf fig 4.6), the results were the same as when I used the Dice Loss. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{segmentation/tverskyloss/attention_map.png}
\caption{The Segmentation Map obtained using the TverskyLoss on the interesting classes for a non pathological patient (left) and a pathological patient (right)}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
  \hline
  Class & Dice Score & Standard Deviation\\
  \hline
  Background & 0.99 & 0.00\\
  Myocardial Infarction & 0.19 & 0.40\\
  No-reflow area & 0.50 & 0.50\\
  \hline
\end{tabular}
\end{center}
\caption{Chart with the dice score for each class}
\end{figure}

\newpage
\subsection{The segmentation with a trainer using the Cross Entropy Loss}

\subsubsection*{Description of the trainer}
I decided to change the loss again, and this time to use a weighted Cross Entropy Loss (CELoss). This loss is defined by the equation 4.3, with $w_i$, $gt_{i}$ and $p_{i}$ respectively the weight, the ground truth and the prediction of the $i^{th}$ class.

\begin{equation}
CELoss = - \sum_{i}^{N} w_{i} \cdot gt_{i} \cdot \log(p_{i})) 
\end{equation}

I choose a weight of 1 for the background, a weight of 100 for the myocardial infarction, and a weight of 150 for the no-reflow area. The weight for the last two classes is significatively higher than the weight for the background because they represent a lot fewer pixels on the image. The weight for the no-reflow area is higher than the weight of the myocardial infarction because it can be empty for some pathological patients. 

For this training, I still used a UNet model and trained it on 600 epochs. It took 5.5 hours (it took more time as I added some data augmentation transformations to prevent an overfitting in the neural network). 

\subsubsection*{The results}
We can see an improvement from the other training on the segmentation maps (see fig. 4.7). The network manages to segment three parts in the MRI. However, it detects a myocardial infarction for a non-pathological patient and the segmented myocardial infarction for a pathological patient is wrong.

Those results can also be seen on the dice scores (see fig. 4.8). They are close to zero.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=\textwidth]{segmentation/celoss/attention_map.png}
\caption{The Segmentation Map obtained using the CELoss on the interesting classes for a non pathological patient (left) and a pathological patient (right)}
\end{center}
\end{figure}

\begin{figure}[!h]
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
  \hline
  Class & Dice Score & Standard Deviation\\
  \hline
  Background & 0.97 & 0.00\\
  Myocardial Infarction & 0.026  & 0.02\\
  No-reflow area & 0.003 & 0.005\\
  \hline
\end{tabular}
\end{center}
\caption{Chart with the dice score for each class}
\end{figure}

\subsection{Conclusion}

The segmentation obtained with a Cross Entropy Loss on the interesting labels is inferior to the segmentation obtained with a Dice Loss on all the labels. 

For the classification, we will use the segmentation maps obtained after the training on all labels with a UNet model, a Dice Loss, and on 600 epochs. 

\end{spacing}